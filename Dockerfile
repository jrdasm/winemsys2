FROM debian:12
ARG BUILDVER
# https://github.com/msys2/MSYS2-packages/issues/682#issuecomment-1850886013
ARG WINE_REPO=msys2-hacks-17:https://gitlab.winehq.org/jhol/wine.git
# https://repo.msys2.org/distrib/x86_64/
ARG MSYS2_VERSION=20231026
# https://wiki.winehq.org/Building_Wine#Satisfying_Build_Dependencies
# https://wiki.winehq.org/Building_Wine#Shared_WoW64
ENV DEBIAN_FRONTEND=noninteractive
RUN dpkg --add-architecture i386 \
 && apt-get update \
 && apt-get install -y \
    build-essential \
    flex \
    bison \
    gettext \
    git \
    gcc-multilib \
    gcc-mingw-w64 \
    libasound2-dev libasound2-dev:i386 \
    libpulse-dev libpulse-dev:i386 \
    libdbus-1-dev libdbus-1-dev:i386 \
    libfontconfig-dev libfontconfig-dev:i386 \
    libfreetype-dev libfreetype-dev:i386 \
    libgnutls28-dev libgnutls28-dev:i386 \
    libgl-dev libgl-dev:i386 \
    libunwind-dev libunwind-dev:i386 \
    libx11-dev libx11-dev:i386 \
    libxcomposite-dev libxcomposite-dev:i386 \
    libxcursor-dev libxcursor-dev:i386 \
    libxfixes-dev libxfixes-dev:i386 \
    libxi-dev libxi-dev:i386 \
    libxrandr-dev libxrandr-dev:i386 \
    libxrender-dev libxrender-dev:i386 \
    libxext-dev libxext-dev:i386 \
    libosmesa6-dev libosmesa6-dev:i386 \
    libsdl2-dev libsdl2-dev:i386 \
    libudev-dev libudev-dev:i386 \
    libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev \
 && apt-get install -y libgstreamer1.0-dev:i386 libgstreamer-plugins-base1.0-dev:i386
WORKDIR /src/wine
# wine installation (in /usr/local)
RUN git clone --depth 1 -b $(echo "$WINE_REPO"|cut -d: -f1) $(echo "$WINE_REPO"|cut -d: -f2-) /src/wine/sources \
 && mkdir -p wine64 wine32 \
 && nbproc=$(grep processor /proc/cpuinfo |wc -l) \
 && cd wine64 \
 && ../sources/configure --enable-win64 --disable-tests \
 && make -j$nbproc \
 && cd ../wine32 \
 && ../sources/configure --with-wine64=../wine64 --disable-tests \
 && make -j$nbproc \
 && make install \
 && cd ../wine64 \
 && make install \
 && mkdir -p /wine \
 && cd /wine \
 && rm -rf /src
# wine installed in /usr/local/bin/
WORKDIR /wine
ENV WINEPREFIX=/wine \
    WINEDEBUG=-all \
    MSYSTEM=MINGW64 \
    TERM=xterm-256color
# msys2 installation
RUN wineboot \
 && wineserver -w \
 && cd drive_c \
 && curl -sSL https://repo.msys2.org/distrib/x86_64/msys2-base-x86_64-${MSYS2_VERSION}.tar.xz|tar -x --lzma -f - \
 && wine64 c:/msys64/usr/bin/bash.exe --login -c echo
ENV WINEPATH='c:\msys64\usr\bin'
RUN printf "#!%s\n%s\n" "/bin/sh" 'wine64 bash.exe -l -i "$@"' > /usr/local/bin/winebash \
 && chmod +x /usr/local/bin/winebash
RUN winebash -c 'pacman --verbose --noconfirm --ignore pacman -S base-devel gcc'
ENTRYPOINT /usr/local/bin/winebash
